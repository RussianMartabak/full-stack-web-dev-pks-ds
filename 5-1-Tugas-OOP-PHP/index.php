<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php

//enable error display
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
//

abstract class Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    //method
    abstract public function __construct($nama);
    public function atraksi() {
        echo $this->nama . " sedang " . $this->keahlian;
    }
}

trait getInfo{
    public function getInfoHewan(){
        echo $this->nama . "<br>";
        echo $this->darah . "<br>";
        echo $this->jumlahKaki . "<br>";
        echo $this->keahlian . "<br>";
        echo $this->attackPower . "<br>";
        echo $this->defencePower . "<br>";
        echo get_class($this) . "<br>";
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;
    public function serang($target) {
        echo $this->nama . " sedang menyerang " . $target->nama;
        $target->diserang($this);
    }
    public function diserang($penyerang) {
        $this->darah -= $penyerang->attackPower / $this->defencePower; 
        echo $this->nama . " sedang  diserang";
    }
}

//actual construction
class Elang extends Hewan{
    use Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencepower = 5;
    }
    use getinfo;
}

class Harimau extends Hewan{
    use Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencepower = 8;
    }
    use getinfo;
}

?>
</body>
</html>
