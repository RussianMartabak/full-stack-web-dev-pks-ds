<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersandRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $uuid = Str::uuid();
        DB::table('roles')->insert([
            'name' => 'Executor',
            'id' =>  $uuid,
        ]);
        DB::table('users')->insert([
            'name' => "Zaky",
            'email' => "zaky".'@gmail.com',
            "username" => "zaky10",
            "role_id" => $uuid,
            "id" => Str::uuid()
        ]);
       
    }
}
