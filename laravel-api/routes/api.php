<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController');
    Route::post('regenerate_otp', 'RegenerateOtpCodeController')->name('auth.regenerate_otp');
    Route::post('verify', 'VerificationController')->name('auth.verify');
    Route::post('update_password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});

Route::group(['middleware' => ['auth:api']], function() {
    Route::apiResource('/post', 'PostController');
    Route::apiResource('/role', 'RolesController');
    Route::apiResource('/comment', 'CommentController');
});


