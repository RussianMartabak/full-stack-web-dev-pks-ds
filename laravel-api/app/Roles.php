<?php

namespace App;

use App\Http\Traits\uuidBoot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Roles extends Model
{
    //
    protected $table = 'roles';
    protected $fillable = ['name', 'id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    use uuidBoot;
}
