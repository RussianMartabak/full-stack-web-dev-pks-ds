<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Http\Traits\uuidBoot;

class OtpCode extends Model
{
    //
    protected $table = 'otp_codes';
    protected $fillable = ['otp', 'user_id', 'valid_until'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    use uuidBoot;
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
