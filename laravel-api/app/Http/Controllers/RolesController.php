<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();
         //make JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $roles
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //find it
        $role = Roles::create([
            'name' => $request['name']
        ]);
        //if found
        if($role) {
            return response()->json([
                'success' => true,
                'message' => "save successful",
                'data' => $role
            ], 201);

        }
        return response()->json([
            'success' => false,
            'message' => 'Failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $role = Roles::findOrFail($id);
        if($role) {
            $role->update([
                'name' => $request['name'],
            ]);
            return response()->json([
                'success' => true,
                'message' => 'roles updated',
                'data' => $role
            ], 200);
        }
        //cant update
        return response()->json([
            'success' => false,
            'message' => 'Data not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Roles::findOrfail($id);

         if($role) {
 
             //delete role
             $role->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Role Deleted',
             ], 200);
 
         }
 
         //data role not found
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);
     
    }
}
