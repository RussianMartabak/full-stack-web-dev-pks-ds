<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //validator
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'otp' => 'required'
        ]);
        //error
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //find otp
        $otp = OtpCode::where('otp', $request['otp'])->first();
        if (!$otp) {
            return response()->json([
                'success' => false,
                'message' => "invalid otp code"
            ], 400);
        }
        $now = Carbon::now();
        if ($now > $otp->valid_until) {
            return response()->json([
                'success' => false,
                'message' => "otp code no longer valid"
            ], 400);
        }

        $user = User::find($otp->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);
        $otp->delete();
        return response()->json([
            'success' => true,
            'message' => "user has been verified"
        ], 200);
    }
}
