<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\RegenOtpMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'email' => 'required'
        ]);
        //error
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //get if email there
        $user = User::where('email', $request['email'])->first();
        //if user not found 
        if(empty($user)) {
            return response()->json([
                'success' => false,
                'message' => 'no user with that email is found'
            ], 404);
        }
        //regen otp
        if($user->otp_code) {
            $user->otp_code->delete();
        }
        
        do {
            $otp = mt_rand(100000, 999999);
            $otp_exist = OtpCode::where('otp', $otp)->first();
        }
        while($otp_exist);
        $now = Carbon::now();
        
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);
        Mail::to($user->email)->queue(new RegenOtpMail($otp_code->otp));
        return response()->json([
            'success' => true,
            'message' => "Otp has been generated",
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);

    }
}
