<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\RegisteredMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        //
         //stroeere
         $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',

            
        ]);
        //error
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //save
        $user = User::create($request->all());
        //generate otp
        do {
            $otp = mt_rand(100000, 999999);
            $otp_exist = OtpCode::where('otp', $otp)->first();
        }
        while($otp_exist);
        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);
        //send mail with the otp
        Mail::to($user->email)->queue(new RegisteredMail($otp_code->otp));
        return response()->json([
            'success' => true,
            'message' => "User created successfully",
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
        

    }
}
