<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::latest()->get();
         //make JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $posts
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //stroeere
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'content' => 'required',
            
        ]);
        //error
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save
        $post = Posts::create([
            'judul' => $request['judul'],
            'content' => $request['content'],
            'user_id' => auth()->user()->id
        ]);
        //save succeed???
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => "save successful",
                'data' => $post
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'Failed to save'
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $post)
    {
        //
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'content' => 'required',
        ]);
        // invadlid req
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //update
        $post = Posts::findOrFail($post->id);
        $user = auth()->user();
        if ($user->id != $post->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'This is not your post'
            ] ,401);
        }

        if($post) {
            $post->update([
                'judul' => $request['judul'],
                'content' => $request['content']
            ]);
            return response()->json([
                'success' => true,
                'message' => 'db updated',
                'data' => $post
            ], 200);
        }
        //cant update
        return response()->json([
            'success' => false,
            'message' => 'Data not found'
        ], 404);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //find post by ID
         $post = Posts::findOrfail($id);
         $user = auth()->user();
         if ($user->id != $post->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'This is not your post'
            ] ,401);
        }
         if($post) {
 
             //delete post
             $post->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Post Deleted',
             ], 200);
 
         }
 
         //data post not found
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);
     
    }
}
