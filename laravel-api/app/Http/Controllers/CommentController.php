<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Mail\PostCreatedMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comments::latest()->get();
        //make JSON
        return response()->json([
           'success' => true,
           'message' => 'List Data Post',
           'data' => $comments
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //find it
        $comment = Comments::create([
            'content' => $request['content'],
            'post_id' => $request['post_id'],
            'user_id' => auth()->user()->id
        ]);

        Mail::to($comment->post->user->email)->send(new PostCreatedMail($comment));

        //if save succedd
        if($comment) {
            
            return response()->json([
                'success' => true,
                'message' => "save successful",
                'data' => $comment
            ], 201);

        }
        return response()->json([
            'success' => false,
            'message' => 'Failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $comment = Comments::findOrFail($id);
        if($comment) {
            $comment->update([
                'content' => $request['content']
            ]);
            return response()->json([
                'success' => true,
                'message' => 'comment updated',
                'data' => $comment
            ], 200);
        }
        //cant update
        return response()->json([
            'success' => false,
            'message' => 'Data not found'
        ], 404);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comments::findOrfail($id);

         if($comment) {
 
             //delete role
             $comment->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Comment Deleted',
             ], 200);
 
         }
 
         //data role not found
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);
     
    }
}
