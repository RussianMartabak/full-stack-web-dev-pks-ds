<?php 
namespace App\Http\Traits;
use Illuminate\Support\Str;
trait uuidBoot {
    protected static function boot() {
        parent::boot();
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
?>