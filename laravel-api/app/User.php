<?php

namespace App;

use App\Roles;
use Illuminate\Support\Str;
use App\Http\Traits\uuidBoot;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    //
    use Notifiable;
    protected $table = 'users';
    protected $fillable = ['id', 'username', 'email', 'name', 'role_id', 'email_verified_at', 'password'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->role_id = Roles::where('name', 'author')->first()->id;
        });
    }
    //relationships
    public function comments() {
        return $this->hasMany('App\Comments');
    }
    public function posts() {
        return $this->hasMany('App\Posts');
    }
    public function otp_code(){
        return $this->hasOne('App\OtpCode', 'user_id');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
