<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Posts extends Model
{
    protected $table = 'posts';
    protected $fillable = ['judul','content', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    //this is copy pastable for uuid insertion
    protected static function boot() {
        parent::boot();
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
}
