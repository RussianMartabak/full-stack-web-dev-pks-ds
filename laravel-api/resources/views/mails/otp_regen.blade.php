<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    A new OTP Code has been generated : {{$otp}}. This OTP Code will be valid for 5 minutes. Do not
    share this code with anyone else. If you are not the one requesting the OTP Code, report to
    authority immidiately
</body>
</html>