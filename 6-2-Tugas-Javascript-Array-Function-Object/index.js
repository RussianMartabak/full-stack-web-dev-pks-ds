//Soal 1
let daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
//Jawaban Soal 1
daftarHewan.sort();
daftarHewan.forEach(e => {
    console.log(e);
});

//Soal 2 
//jawaban 2
function introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan);

//Soal 3
//jawaban 3
function hitung_huruf_vokal(string) {
    let vocals = ['a', 'i', 'u', 'e', 'o'];
    let stringArray = string.split('');//break into array
    return stringArray.reduce((prev, curr) => {
        let sum = prev;
        let lowCurr = curr.toLowerCase();//convert the current letter to lowercase
        if (vocals.includes(lowCurr)) {
            sum += 1;
        }
        return sum;
    }, 0)
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4
//jawaban soal 4
function hitung(int) {
    let init = -2;
    return int * 2 + init;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
