import { CategoriesComponent } from "./CatComponent.js";
import { CategoryComponent } from "./CatsComponent.js";
//import Vue from "vue";
//import VueRouter from "vue-router";

// Kita membuat 2 component , yaitu Home dan About
const Home = { template: '<div>Ini Halaman Home</div>' };
const About = { template: '<div>Ini Halaman About</div>' };

const routes = [
    { path: '/', component: Home , alias: '/home'},
    { path: '/about', component: About, meta: {login: true} },
    { path: '/category/:id', component: CategoryComponent },
    { path: '/categories', component: CategoriesComponent },
  ];

const router = new VueRouter({
  mode: 'history',
  routes
});



var vm = new Vue({ 
  el: '#app', 
  router,
  methods: {
    getParams() {
      console.log(this.$route.params);
    }
  }
 });